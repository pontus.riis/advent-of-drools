package com.advent.drools;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;


class Day1Test {

    private static final String K_SESSION_NAME = "AdventKS";
    private KieSession kieSession;

    @BeforeEach
    void before() {
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        kieSession = kContainer.newKieSession(K_SESSION_NAME);

        Logger ruleLogger = LoggerFactory.getLogger(K_SESSION_NAME);
        kieSession.setGlobal("log", ruleLogger);
    }

    @Test
    void testChallenge1() throws URISyntaxException, IOException {
        Path path = Paths.get(this.getClass().getResource("/2019/day1/modules.txt").toURI());
        Stream<String> lines = Files.lines(path);
        lines.map(Integer::parseInt)
                .map(Module::new)
                .forEach(m -> kieSession.insert(m));

        kieSession.fireAllRules();

        Collection<?> objects = kieSession.getObjects(o -> o instanceof TotalFuel);

        System.out.println(objects.stream().map(o1 -> (TotalFuel) o1).collect(Collectors.toList()).get(0).getValue());
    }

}
